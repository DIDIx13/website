<?php
    namespace dtw\user;

    class Notifications {
        private $notifications;
    
        public function __construct($id) {
            // Register API hook
            \dtw\DtW::$api->registerHook('notifications', 'unread', function($params) {
                $DtW = \dtw\DtW::getInstance();

                $response = array(
                    'count' => $this->unread,
                    'pm' => \dtw\messages\Messages::getUnread()
                );

                if (isset($_GET['full'])) {
                    $notifications = $this->get(true);

                    if (count($notifications)) {
                        $response['unread'] = array();
                        $response['unread']['today'] = array();
                        $response['unread']['yesterday'] = array();
                        $response['unread']['older'] = array();

                        // Filter notifications into date segments
                        foreach($notifications AS $notification) {
                            $segment = 'older';


                            if (date('Ymd') == date('Ymd', $notification->created)) {
                                $segment = 'today';
                            } else  if (date('Ymd', strtotime('-1 day')) == date('Ymd', $notification->created)) {
                                $segment = 'yesterday';
                            }

                            array_push($response['unread'][$segment], $notification->message->full);
                        }
                    }
                }

                return $response;
            });

            \dtw\DtW::$api->registerHook('notifications', 'markread', function($params) {
                $DtW = \dtw\DtW::getInstance();
                return $DtW->user->notifications->markAllRead();
            });

            $this->userID = $id;

            // Get unread notifications from Redis
            $key = 'user:' .  $this->userID . ':notifications';

            if (isset($_SESSION['notifications'])) {
                $notifications = $_SESSION['notifications'];
            } else {
                $notifications = array();
            }

            $newNotifications = \dtw\DtW::$redis->sMembers($key);
            if ($newNotifications) {
                // Decode notifications
                $newNotifications = array_map(function($data) {
                    return json_decode($data);
                }, $newNotifications);

                // Add them to user session
                $notifications = array_merge($notifications, $newNotifications);
                $_SESSION['notifications'] = $notifications;

                // Remove from Redis
                \dtw\DtW::$redis->del($key);
            }

            $this->unread = count($notifications);
        }

        public function load($unread = true) {
            // Remove Redis cache
            \dtw\DtW::$redis->del($key);

            // Delete expired notifications
            $sql = 'DELETE FROM user_notifications WHERE `to` = :userID AND `created` < now() - interval 30 day AND `read` > 0';
            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute(array(':userID' =>  $this->userID)); 

            // Load notifications
            $sql = 'SELECT `notification_id` AS `id`, `to`, `from`, `type`, `data`, `created`, `read` FROM user_notifications WHERE `to` = :userID';
            if ($unread) {
                $sql .= ' AND `read` IS NULL';
            }
            $sql .= ' ORDER BY `created` DESC';

            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute(array(':userID' =>  $this->userID)); 
            if ($stmt->rowCount()) {
                $notifications = $stmt->fetchAll();
                if ($unread) {
                    $_SESSION['notifications'] = $notifications;
                }
                return $notifications;
            }
        }

        public function get($unread = false) {
            if ($unread) {
                $notifications = $_SESSION['notifications'];
            } else {
                // Load all notifications
                $notifications = $this->load(false);
            }

            if (!$notifications || !count($notifications)) {
                return null;
            }

            $notifications = array_map(function($data) {
                return new \dtw\Notification($data);
            }, $notifications);

            return $notifications;
        }

        public function markAsRead($id) {
            // Find id in unread notifications
            $notifications = $this->get(true);

            if (!$notifications || !count($notifications)) {
                return;
            }

            $found = null;
            foreach($notifications as $notification) {
                if ($id == $notification->id) {
                    $found = $notification;
                    break;
                }
            }

            if (!$found) return;

            // Mark as read
            $found->markAsRead();

            // Remove from session
            $notifications = array_filter($_SESSION['notifications'], function($notification) use ($id){
                return ($id != $notification->id);
            });

            $_SESSION['notifications'] = $notifications;
            $this->unread = count($notifications);
        }

        public function markAllRead() {
            unset($_SESSION['notifications']);

            // Remove from Redis (just in case)
            \dtw\DtW::$redis->del($key);

            // Mark notifications as read in DB
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE user_notifications
                SET `read` = now()
                WHERE `to` = :userID AND `read` IS NULL
                ORDER BY `created` DESC
            ');
            $stmt->execute(array(
                ':userID' =>  $this->userID
            ));

            return true;
        }
    }
