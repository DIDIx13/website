<?php
    namespace dtw\utils;

    class Utils {
        public static function slug($text) {
            // http://stackoverflow.com/a/2955878
            $text = preg_replace('~[^\pL\d]+~u', '-', $text);
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            $text = preg_replace('~[^-\w]+~', '', $text);
            $text = trim($text, '-');
            $text = preg_replace('~-+~', '-', $text);
            $text = strtolower($text);

            if (empty($text)) {
                return self::generateToken(8);
            }

            return $text;
        }

        public static function unslug($slug) {
            return ucfirst(str_replace("-", " ", $slug));
        }

        public static function generateToken($length = 64) {
            $token = bin2hex(openssl_random_pseudo_bytes(64));

            return substr($token, 0, $length);
        }

        public static function validateURL($url) {
            $urlpart = parse_url($url);
            if (!array_key_exists('scheme', $urlpart)) {
                $url = 'https://' . $url;
            } else if ($urlpart["scheme"] !== "http" && $urlpart["scheme"] !== "https") {
                return;
            }

            $website = filter_var($url, FILTER_SANITIZE_URL);

            if (filter_var($website, FILTER_VALIDATE_URL)) {
                return $website;
            } else{
                return;
            }
        }

        public static function roundDown($value) {
            $place = 10;

            if ($value < 10) {
                return $value;
            } else if ($value > 99999) {
                $place = 100000;
            } else if ($value > 99999) {
                $place = 10000;
            } else if ($value > 9999) {
                $place = 1000;
            } else if ($value > 999) {
                $place = 100;
            }

            $value = floor($value / $place) * $place;

            if ($value > 999999) {
                $value /= 1000000;
                return $value . ' million';
            } else if ($value > 999) {
                $value /= 1000;
                return $value . ' thousand';
            }

            return $value;
        }
    }
?>