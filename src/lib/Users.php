<?php
    namespace dtw;

    class Users {
        public static function registerHooks() {
            // Register API hooks
            if (\dtw\DtW::$api) {
                \dtw\DtW::$api->registerHook('user', 'matrixToken', function($params) {
                    return \dtw\DtW::getInstance()->user->getMatrixToken();
                });

                \dtw\DtW::$api->registerHook('user', 'update', function($params) {
                    \dtw\Medals::awardMedal('Dark side');
                    return \dtw\user\Meta::set('style.dark', $params['darkMode'], true);
                });

                \dtw\DtW::$api->registerHook('user', 'card', function($params) {
                    $profile = \dtw\Users::getUser($_GET['id']);
                    $html = \dtw\DtW::getInstance()->tmpl->render('profile/rollup.twig', array('profile' => $profile, 'popup' => true, 'stats' => true, 'status' => true));

                    return array(
                        'html' => $html
                    );
                });

                \dtw\DtW::$api->registerHook('user', 'bar', function($params) {
                    $profile = \dtw\Users::getUser($_GET['id']);

                    header('Content-type: image/png');
                    echo $profile->getUserBar($_GET['display']);

                    die();
                });

                \dtw\DtW::$api->registerHook('users', 'lookup', function($params) {
                    $q = trim($_GET['q']);
                    $q = preg_replace("/[^0-9A-Za-z_.-]/", "", $q);

                    if (!strlen($q)) {
                        throw new \Exception('No search term');
                    }

                    $users = \dtw\Users::getUsers(array(
                        'q' => $q . '%',
                        'limit' => 5,
                        'orderBy' => 'reputation'
                    ));

                    if (!$users) {
                        throw new \Exception('No results');
                    }

                    return array(
                        'results' => array_map(function($item) {
                            $result = new \stdClass();
                            $result->text = $item->username;
                            $result->replace = $item->username;
                            return $result;
                        }, $users)
                    );
                });

                // External API
                \dtw\DtW::$api->registerHook('user', 'profile', function($params) {
                    return self::apiProfile($params);
                }, true);
            }
        }

        public static function getUser($lookup) {
            if (!$lookup) {
                throw new \Exception('No user details given');
            }

            try {
                $user = new user\Profile($lookup);
            } catch (\Exception $e) {
                throw $e;
            }

            return $user;
        }

        public static function purgeUser($user_id) {
            $DtW = \dtw\DtW::getInstance();
            if ($user_id == $DtW->user->id) {
                // Clear next medal
                $DtW->user->getNextMedal(true);
            }

            // Update users reputation
            $user = self::getUser($user_id);
            $user->calculateReputation(true);
            $user->deleteCache();
        }

        public static function getUsers($args = array()) {
            $params = array();
            $sql = 'SELECT `user_id` AS `id`, `username` FROM users';

            if ($args['q']) {
                $sql .= ' WHERE `username` LIKE :q';
                $params[':q'] = "{$args['q']}";
            }

            $orderByAllows = array('created', 'username', 'reputation');
            if ($args['orderBy'] && in_array(strtolower($args['orderBy']), $orderByAllows)) {
                $order = strtolower($args['order']) == 'asc' ? 'ASC' : 'DESC';
                $orderBy = $args['orderBy'];
                $sql .= " ORDER BY `{$orderBy}` {$order}";
            }

            if ($args['limit']) {
                $limit = intval($args['limit']);
                $sql .= " LIMIT {$limit}";
            } else {
                $sql .= " LIMIT 1000";
            }

            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute($params); 
            if ($stmt->rowCount()) {
                $users = $stmt->fetchAll();

                return $users;
            }
        }

        public static function calculateAllReputation() {
            $sql = 'SELECT `user_id` AS `id` FROM users ORDER BY `user_id` ASC';
            $users = \dtw\DtW::$db->prepare($sql); 
            $users->execute(); 
            while ($uID = $users->fetch(\PDO::FETCH_COLUMN)) {
                // Update reputation
                echo 'Reputation ' . $uID . ' progress...';
                $u = new user\Profile($uID);
                $u->calculateReputation(true, true);
                echo "\n";
            }
        }

        public static function getRegisteredUsers() {
            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `users`'); 
            $stmt->execute();
            return $stmt->fetch(\PDO::FETCH_COLUMN);
        }

        public static function getStats() {
            $key = 'users:stats';
            $stats = \dtw\DtW::$redis->get($key);

            if ($stats) {
                $stats = json_decode($stats);
            } else {
                $stats = array();

                $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `users`'); 
                $stmt->execute();
                $stats['count'] = $stmt->fetch(\PDO::FETCH_COLUMN);

                $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `user_activity` WHERE `active` >= NOW() - INTERVAL 1 WEEK');
                $stmt->execute();
                $stats['active'] = $stmt->fetch(\PDO::FETCH_COLUMN);

                $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `user_activity` WHERE `active` >= NOW() - INTERVAL 15 MINUTE');
                $stmt->execute();
                $stats['online'] = $stmt->fetch(\PDO::FETCH_COLUMN);

                \dtw\DtW::$redis->set($key, json_encode($stats));
                \dtw\DtW::$redis->expire($key, 900);
            }

            return $stats;
        }

        public static function apiProfile() {
            $tempProfile = \dtw\Users::getUser($_GET['id']);

            $fields  = [
                'username',
                'created',
                'reputation',
                'permalink',
                'bio',
                'name',
                'status',
                'stats',
                'levelProgress',
                'followersCount',
                'followingCount'
            ];

            $extras = [
                'medals',
                'levels'
            ];


            if ($params['extras']) {
                foreach(explode(',', $params['extras']) AS $extra) {
                    $extra = trim(strtolower($extra));

                    if (in_array($extra, $extras)) {
                        array_push($fields, $extra);
                    }
                }
            }


            $profile = new \stdClass();

            foreach($fields AS $key) {
                if (
                    $key == 'levelProgress' ||
                    $key == 'followingCount' ||
                    $key == 'followersCount'
                ) {
                    $profile->stats->$key = intval($tempProfile->$key);
                } else {
                    $profile->$key = $tempProfile->$key;
                }
            }

            // Tidy up medals
            if (in_array("medals", $fields)) {
                if (!$profile->medals) {
                    $profile->medals = $tempProfile->getMedals();
                }

                if ($profile->medals) {
                    foreach($profile->medals AS &$m) {
                        $medal = \dtw\Medals::getMedal($m);
                        
                        $m = new \stdClass();
                        $m->label = $medal->label;
                        $m->colour = $medal->colour;
                    }
                }
            }

            // Tidy up levels
            if (in_array("levels", $fields)) {
                foreach($profile->levels AS &$l) {
                    $level = \dtw\Playground::getByID($l->level_id);
                    unset($l->level_id);
                    $l->title = $level->title;

                    if ($level->subject) {
                        $l->subject = $level->subject;
                    }

                    // $l->stats = $level->meta->stats;
                    // unset($l->stats->graph);
                }
            }

            return $profile;
        }

    }