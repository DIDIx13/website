<?php
    namespace dtw\moderation;

    class Users {

        public static function setPrivileges($userID, $privileges) {
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE users
                SET `privileges` = :privileges
                WHERE `user_id` = :id
                    AND (`privileges` IS NULL OR `privileges` != "green")
            ');
            $stmt->execute(array(
                ':id' => $userID,
                ':privileges' => $privileges
            ));

            \dtw\Users::purgeUser($userID);

            // Store update for user if value has changed
            if ($stmt->rowCount()) {
                $key = 'user:' . $userID . ':privileges';
                \dtw\DtW::$redis->set($key, $privileges ? $privileges : 'unset');
                \dtw\DtW::$redis->expire($key, 604800);
            }
        }

    }