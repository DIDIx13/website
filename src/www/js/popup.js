$('[data-popup-target]').on('click', function(e) {
    e.preventDefault();

    var $this = $(this),
        $target,
        target = $this.data('popup-target');
    if (target == 'sibling') {
        $target = $this.siblings(':eq(0)');
    } else {
        $target = $(target);
    }

    // Hide other popups
    $('.popup--open').not($target).removeClass('popup--open');
    $('.popup-handle--active').not($this).removeClass('popup-handle--active');

    if (!$target) {
        return;
    }

    $target.toggleClass('popup--open');

    if ($target.hasClass('popup--open')) {
        $this.addClass('popup-handle--active');
    } else {
        $this.removeClass('popup-handle--active');
    }

    $("body").one('click', function() {

    });

    $("body").on('click.popup', function(e) {
        // if the target of the click isn't the container nor a descendant of the container
        if (!$target.is(e.target) && $target.has(e.target).length === 0) {
            $target.removeClass('popup--open');
            $this.removeClass('popup-handle--active');

            // Remove handler
            $("body").off('.popup');
        }
    });
});