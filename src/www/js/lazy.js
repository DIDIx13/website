if (!document.dtw) {
    document.dtw = {};
}

document.dtw.lazy = function() {
    $('[data-lazy-image-size]').each(function() {
        var $target = $(this),
            image = $target.attr('src'),
            bg = false;

        if (!image) {
            bg = true;
            image = $target.css('background-image');
            image = image.replace('url(','').replace(')','').replace(/\"/gi, "");
        }

        var size = $target.data('lazy-image-size');
        var newImage = image.replace(/\/uploads\/\w+\//gi, '/uploads/'+size+'/');

        var tmpImg = new Image();
        tmpImg.src = newImage;
        tmpImg.onload = function() {
            if (bg) {
                $target.css('background-image', 'url("'+newImage+'")');
            } else {
                $target.attr('src', newImage);
            }
        };
    });

    $('[data-image-alternative]').on('mouseenter mouseleave', function() {
        var $target = $(this),
            image = $target.attr('src'),
            newImage = $target.data('image-alternative');

        var tmpImg = new Image();
        tmpImg.src = newImage;
        tmpImg.onload = function() {
            $target.attr('src', newImage);
            $target.data('image-alternative', image);
        };
    });
};

$(function() {
    document.dtw.lazy();
});