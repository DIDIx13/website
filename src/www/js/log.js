window.onerror = function(msg, url, line) {
    $.post('/api/1/log/error', {
        msg: msg,
        url: url,
        line: line
    });
};