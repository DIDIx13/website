As a Defend the Web member:

* Answers to all levels will be my own work, unless otherwise instructed.
* I will not share complete answers to any level.
* I will not participate in, condone or encourage unlawful activity, including any breach of copyright, defamation, or contempt of court.
* I will not ‘spam’ other Defend the Web members by posting the same message multiple times or posting a message that is unrelated to the discussion.
* As the Defend the Web community’s first language is English, I will always post contributions in English to enable all to understand
* I will not use Defend the Web to advertise products or services for profit or gain.
* I will not use racist, sexist, homophobic, sexually explicit or abusive terms or images, or swear words or language that is likely to cause offence.