<?php
    $this->respond(function($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
    });

    $this->respond('GET', 's', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        $breadcrumb = array(
            'Messages' => '/messages'
        );

        if (isset($_GET['page'])) {
            $page = intval($_GET['page']);
        }
        if (!$page || !is_numeric($page)) {
            $page = 1;
        }

        $messages = \dtw\messages\Messages::getAll($page);

        if (count($messages) == 0) {
            $response->redirect('/messages/new')->send();
            $this->skipRemaining();
        }

        return $app->DtW->tmpl->render('messages/list.twig', array('breadcrumb' => $breadcrumb, 'messages' => $messages, 'page' => $page));
    });

    $this->respond('GET', 's/new', function ($request, $response, $service, $app) {
        $app->DtW->user->canContribute($response, $service);

        if (isset($_GET['to'])) {
            // Lookup user and see if we have an open conversation with them
            $to = \dtw\Users::getUser($_GET['to']);
            $existingConvo = \dtw\messages\Messages::getConversationByRecipient($to->id);
            
            if ($existingConvo) {
                $response->redirect('/message/' . $existingConvo)->send();
                return;
            }
        }

        $breadcrumb = array(
            'Messages' => '/messages'
        );

        try {
            $messages = \dtw\messages\Messages::getAll();

            // New message form
            $form = new \dtw\utils\Form("New message", "Send", array(
                'hideTitle' => true,
                'block' => false,
                'draft' => 'discussion:' . $conversation->id
            ));
            $form->addField('Recipients', "input", array( 'autocomplete' => true, 'value' => $_GET['to'] ));
            $form->addField('Message', "markdown");

            return $app->DtW->tmpl->render('messages/list.twig', array('breadcrumb' => $breadcrumb,
                'messages' => $messages,
                'form' => $form
            ));
        } catch (\Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/messages')->send();
        }
    });

    $this->respond('POST', 's/new', function ($request, $response, $service, $app) {
        $app->DtW->user->canContribute($response, $service);

        try {
            $conversation = \dtw\messages\Messages::newConversation($_POST['recipients'], $_POST['message']);

            $response->redirect('/message/' . $conversation)->send();
            $this->skipRemaining();
        } catch (\Exception $e) {
            // Restore form data
            \dtw\utils\Form::storeResponses();

            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $service->back();

            $response->send();
            $this->skipRemaining();
        }
    });

    $this->respond('GET', '/[:conversation]', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Messages' => '/messages'
        );

        try {
            if (isset($_GET['page'])) {
                $page = intval($_GET['page']);
            }
            if (!$page || !is_numeric($page)) {
                $page = 1;
            }

            $messages = \dtw\messages\Messages::getAll($page);

            $conversation = \dtw\messages\Messages::getConversation($request->conversation);
            $conversation->load();
            $conversation->markAsRead();

            // Reply form
            $form = new \dtw\utils\Form("Reply", "Send", array(
                'action' => '/message/' . $conversation->id,
                'hideTitle' => true,
                'block' => false,
                'draft' => 'discussion:' . $conversation->id
            ));
            $form->addField('Reply', "markdown");

            $template = 'messages/list.twig';
            if (isset($_GET['ajax'])) {
                $template = 'messages/list-conversation.twig';
            }

            return $app->DtW->tmpl->render($template, array('breadcrumb' => $breadcrumb,
                'messages' => $messages,
                'conversation' => $conversation,
                'form' => $form,
                'page' => $page
            ));
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/messages')->send();
        }
    });

    $this->respond('POST', '/[:conversation]', function ($request, $response, $service, $app) {
        $app->DtW->user->canContribute($response, $service);

        try {
            $conversation = \dtw\messages\Messages::getConversation($request->conversation);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/messages')->send();
            return;
        }

        if (isset($_GET['action'])) {

            if ($_GET['action'] == 'delete') {
                $conversation->markAsDeleted();

                \dtw\utils\Flash::add('Conversation deleted', 'success');
                $response->redirect('/messages')->send();
                $this->skipRemaining();
            }

        } else {
            try {
                $conversation->addReply($_POST['reply']);
                \dtw\utils\Flash::add('Reply added', 'success');
                $response->redirect('/message/' . $conversation->id)->send();
                $this->skipRemaining();
            } catch (Exception $e) {
                // Restore form data
                \dtw\utils\Form::storeResponses();

                \dtw\utils\Flash::add($e->getMessage(), 'error');
                $response->redirect('/message/' . $conversation->id)->send();
            }
        }

    });