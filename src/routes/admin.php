<?php
    $this->respond('GET', '', function($request, $response, $service, $app) {
        try {
            $app->DtW->load('Admin');
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $breadcrumb = array(
            'Admin' => '/admin'
        );

        $args = array('breadcrumb' => $breadcrumb);

        $args['redisInfo'] = \dtw\DtW::$redis->info();

        $app->DtW->load('Search');
        $args['searchInfo'] = $app->DtW->search->info();

        $args['log'] = \dtw\DtW::$log->getLog(true);

        $args['stats'] = $app->DtW->admin->getStats();

        return $app->DtW->tmpl->render('admin/utils.twig', $args);
    });

    $this->respond('POST', '', function($request, $response, $service, $app) {
        try {
            $app->DtW->load('Admin');
        } catch (Exception $e) {
            $this->skipRemaining();
            return;
        }

        $app->DtW->load('Search');

        switch ($_GET['action']) {
            case 'flushAll': $app->DtW->admin->flushAll(); break;
            case 'flushSearch': $app->DtW->search->reseed(); break;
            // case 'calculateReputation': \dtw\Users::calculateAllReputation(); break;
        }
            
        $response->redirect('/admin')->send();
        $klein->skipRemaining();
    });