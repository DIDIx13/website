<?php
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Styles' => '/styles'
        );

        return $app->DtW->tmpl->render('styles.twig', array('breadcrumb' => $breadcrumb));
    });