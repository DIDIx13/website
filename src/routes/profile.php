<?php
    $this->respond('GET', '/[:user]?', function ($request, $response, $service, $app) {
        if (!$request->user) {
            $app->DtW->user->isAuth($response);

            $request->user = $app->DtW->user->id;
        }

        try {
            $profile = \dtw\Users::getUser($request->user);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/profile');
        }

        $breadcrumb = array(
            'Profiles' => '/profile'
        );

        if ($request->user === $app->DtW->user->id) {
            $breadcrumb[$profile->username] = '/profile';
        } else {
            $breadcrumb[$profile->username] = '/profile/' . $profile->username;
        }

        return $app->DtW->tmpl->render('profile/index.twig', array('breadcrumb' => $breadcrumb, 'profile' => $profile ));
    });

    $this->respond('POST', '/[:user]?', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        if (!$request->user) {
            $request->user = $app->DtW->user->id;
        }

        try {
            $profile = \dtw\Users::getUser($request->user);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/profile');
        }

        if ($app->DtW->user->hasPrivilege('users.regenerate')) {
            if ($_GET['moderation'] == 'regenerate') {
                $profile->calculateReputation(true, true);
                \dtw\utils\Flash::add('Profile regenerated', 'success');
            }
        }

        if ($app->DtW->user->hasPrivilege('users.block')) {
            if ($_GET['moderation'] == 'block') {
                \dtw\moderation\Users::setPrivileges($profile->id, 'black');
                \dtw\utils\Flash::add('User blocked', 'success');
            } else if ($_GET['moderation'] == 'unblock') {
                \dtw\moderation\Users::setPrivileges($profile->id, null);
                $profile->calculateReputation(true, true);
                \dtw\utils\Flash::add('User unblocked', 'success');
            }
        }

        $service->back();
        $response->send();
    });

    $this->respond('POST', '/[:user]/[follow|unfollow|block|unblock:action]', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        try {
            $profile = \dtw\Users::getUser($request->user);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/profile');
        }

        try {
            if ($request->action === 'follow') {
                $profile->follow();
                \dtw\utils\Flash::add('You have started following ' . $profile->username , 'success');
            } else if ($request->action === 'unfollow') {
                $profile->unfollow();
                \dtw\utils\Flash::add('You have stopped following ' . $profile->username , 'success');
            } else 
            if ($request->action === 'block') {
                $profile->block();
                \dtw\utils\Flash::add('You have blocked ' . $profile->username , 'success');
            } else if ($request->action === 'unblock') {
                $profile->unblock();
                \dtw\utils\Flash::add('You have unblocked ' . $profile->username , 'success');
            }
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        if (!isset($_GET['ajax'])) {
            $service->back();
            $response->send();
        } else {
            \dtw\utils\Flash::load();
            $flash = \dtw\utils\Flash::get();
            if ($flash) {
                echo json_encode($flash[0]);
            }
        }
    });