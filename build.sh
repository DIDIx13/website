#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

printf "         ____  __ _       __\n        / __ \/ /| |     / /\n       / / / / __/ | /| / / \n      / /_/ / /_ | |/ |/ /  \n     /_____/\__/ |__/|__/   \n\n\n"

configCheck () {
    printf "Checking config... "
    CONFIG=./config/config.json
    EXAMPLE=./config/config.json.example

    if [ -f $CONFIG ]; then
        printf "${GREEN}file exists${NC}\n"
    else
        printf "${RED}file missing${NC}\n"

        if configGenerate; then
            configExample
        fi
    fi
}

configExample () {
    read -p 'Use example config file? [Y/n] ' create

    if [ "$create" = '' ]; then
        create="Y"
    fi

    case $create in
        [Yy]* )  echo "Creating config"; cp ${EXAMPLE} ${CONFIG};;
        * ) echo "Ok, fine"
    esac
}

configGenerate () {
    read -p 'Generate config file? [Y/n] ' create

    if [ "$create" = '' ]; then
        create="Y"
    fi

    case $create in
        [Nn]* ) return 0
    esac

    read -p 'Domain: ' domain;
    echo "TODO: finish generator"

    return 1
}

dirCheck () {
    folders=( "site/staticCache" "logs" "sessions" "site/templates/cache" "uploads" "site/www/imgs/uploads" )
    for i in "${folders[@]}"
    do
        mkdir -pm 777 $i
        printf "Creating ${i}... ${GREEN}done${NC}\n"
    done
}

buildCheck () {
    printf "Checking composer... "
    if ! [ -x "$(command -v composer)" ]; then
        printf "${RED}not installed${NC}\n"
        exit 1
    else
        printf "${GREEN}found${NC}\n"  
    fi

    printf "Checking NPM... "
    if ! [ -x "$(command -v npm)" ]; then
        printf "${RED}not installed${NC}\n"
        exit 1
    else
        printf "${GREEN}found${NC}\n"  
    fi
}


build () {
    buildCheck

    composer install
    npm install
    ./node_modules/.bin/gulp build
    ./node_modules/.bin/gulp images
}



configCheck
dirCheck
build